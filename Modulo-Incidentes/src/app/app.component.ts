import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import { PrincipalService } from './Servicios/principal.service';

import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {

  private gridApi;
  private gridColumnApi;

  private columnDefs;
  private defaultColDef;
  private rowSelection;
  private rowData: any[];
  public listado: any[];
  public textoIncidenteSeleccionado: string = "Seleccionar Incidente";
  private textoBusqueda:string = "Sin texto";

  values = '';

  constructor(private servicioPrincipal:PrincipalService, private spinner: NgxSpinnerService) {
  
    this.columnDefs = [
      { headerName: "Id", field: "Id", minWidth: 70,  maxWidth: 90,sortable: true, filter: true },
      { headerName: "Tema", field: "Tema",  minWidth: 150,sortable: true, filter: true },
      { headerName: "Texto", field: "Texto", minWidth: 200,sortable: true, filter: true },
      { headerName: "Cliente", field: "Cliente", sortable: true, filter: true },
      { headerName: "CantidadDeNotas", field: "CantidadDeNotas", sortable: true, filter: true },
      { headerName: "Prioridad", field: "Prioridad", sortable: true, filter: true },
      { headerName: "Programador", field: "Programador", sortable: true, filter: true },
      { headerName: "Tipo", field: "Tipo", sortable: true, filter: true },
      { headerName: "Especializacion", field: "Especializacion", sortable: true, filter: true },
      { headerName: "Opcion", field: "Opcion", sortable: true, filter: true },
      { headerName: "Estado", field: "Estado", sortable: true, filter: true },
      { headerName: "Ingresado", field: "Ingresado", sortable: true, filter: true },
      { headerName: "Version", field: "Version", sortable: true, filter: true },
      { headerName: "UsuGenerador", field: "UsuGenerador", sortable: true, filter: true },
      { headerName: "Visibilidad", field: "Visibilidad", sortable: true, filter: true },
      { headerName: "Sprint", field: "Sprint", sortable: true, filter: true },

    ];

    this.defaultColDef = {
      flex: 1,
     minWidth: 100,
    };
    this.rowSelection = 'single';

    // this.servicioPrincipal.TraerIncidentesPorTexto("weborama")
    // .then((resp: any[]) => {
    //  console.log(resp[2])
    //  this.rowData = resp[2];
    // });

    this.rowData = [];

  }

  onKey(event: any) { 
    this.textoBusqueda = null;
    this.textoBusqueda = event.target.value;
  }

  BuscarIncidentes(){

    this.servicioPrincipal.TraerIncidentesPorTexto(this.textoBusqueda)
    .then((resp: any[]) => {
     console.log(resp[2])
     this.rowData = resp[2];
    });

  }

  ngOnInit() {
    this.spinner.show();
 
    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

  onSelectionChanged(evento: any) {
    var filaSeleccionada = evento.api.getSelectedRows();
    console.log(filaSeleccionada[0].Texto); 
    this.textoIncidenteSeleccionado = filaSeleccionada[0].Texto;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
}
