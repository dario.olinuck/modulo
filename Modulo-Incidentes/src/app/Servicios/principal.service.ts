import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  url: string = "http://prua:8080/apiIncidentes/public/getList/";

  constructor(private http: HttpClient) { }
 
  TraerIncidentesPorTexto(parametros: string) {
    return this.http.get(this.url + parametros).toPromise()
  }
}
